#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Dada la estructura de datos:
 * typedef stuct{
 *      char nombre[40];
 *      int dui;
 *      float sueldo;
 * } empleado;
 * 
 * Escribir un programa que haga lo siguiente:
 * a) Declare un array de estructuras con una dimension de 1000 empleados;
 * b) Captura de teclado por medio de un bucle apropiado un numero indeterminado de registros
 *    de empleados.
 * c) Ordenar el array por el primer campo y lo imprime en pantalla.
 * d) Calcule e imprima el promedio de los sueldos.
*/

typedef struct {
    char nombre[40];
    int dui;
    float sueldo;
} Empleado;

void ingresar_empleados();
void ordenar_por_nombre();
void calcular_promedio();
void imprimir();

int main(){
    Empleado empleado[1000];
    int exit = 0, opcion;
    while (exit != 1){
        printf("\t\tSistema de empleados\nQue desea hacer?\n");
        printf("1.- Ingresar empleados\n2.-Mostrar empleados\n3.-Calcular promedio de sueldos\n4.-Salir\n");
        printf("Escriba el numero de la accion a realizar: ");
        scanf("%d", &opcion);
        switch (opcion){
        case 1:
            printf("Ingresar empleados");
            //ingresar_empleados();
            break;
        case 2:
            printf("Ordenar empleados");
            // ordenar_por_nombre();
            break;
        case 3:
            printf("Calcular empleados");
            // calcular_promedio();
            break;
        case 4:
            printf("Bye bye\n");
            exit = 1;
            break;
        default:
            printf("OPCION NO VALIDA\n");
            break;
        }
    }
    return 0;
}
