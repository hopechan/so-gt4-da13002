#include <stdio.h>
#include <stdlib.h>

/**
 * Desarrolle un programa que pida una serie de numeros al usuario y halle el maximo, el minimo
 * y la media aritmetica de ellos. Para ello se debe crear una variable puntero tipo float,
 * se debe pedir al usuario que introduzca el numero de datos, y despues los datos a operar.
 * Recuerde que se debe reservar memoria de forma dinamica para almacenar el vector de los datos
*/

// Declaracion de funciones
float max(int n, float *numeros);
float min(int n, float *numeros);
float avg(int n, float *numeros);

int main(){
    int n; 
    float *total;
    printf("\nCalculo de maximos, minimos y media aritmetica :\n"); 
	printf("--------------------------------------------------\n");	 
    printf("Ingrese cuantos numeros desea operar: ");
    scanf("%d", &n);
    total = (float* ) malloc(n *sizeof(float));

    // Se llena con valores ingresados por el usuario
    for (int i = 0; i < n; i++){
        printf("\033[1;32m"); // Color verde
        printf("Ingrese el valor #%d: ", (i+1));
        scanf("%f", (total+i));
    }

    printf("\033[0m");
    printf("Resultados\n");
    printf("--------------------------------------------------\n");

    printf("\033[0;31m"); // Color rojo
    printf("El valor minimo es: %0.2f\n", min(n, total));

    printf("\033[0;34m"); // Color azul
    printf("El valor maximo es: %0.2f\n", max(n, total));

    printf("\033[0;35m"); // Color magenta
    printf("La media aritmetica es: %0.2f\n", avg(n, total));
    return 0;
}

float max(int n, float *numeros){
    float max = *numeros;
    for (int i = 0; i < n; i++){
        if(*(numeros+i) > max){
            max = *(numeros+i);
        }
    }
    return max;     
}

float min(int n, float *numeros){   
    float min = *numeros;
    for (int i = 0; i < n; i++){
        if(*(numeros+i)< min){
            min = *(numeros +1);
        }
    }
    return min;
}

float avg(int n, float *numeros){
    float avg, suma=0;
    for (int i = 0; i <= n; i++){
        suma = suma + *(numeros + i);
    }
    avg = (float) suma/n;
    return avg;
}