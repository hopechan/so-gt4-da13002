#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/**

 * Este ejercicio consiste en hacer un programa que cuente cuantas veces se repiten
 * las vocales (se deben contar las mayusculas y minusculas) en un texto dado por el 
 * usuario. La estructura del programa debe ser el siguiente:
 * #include <stdio.h>
 * #include <stdlib.h>
 * char *pedirTexto();
 * void contarVocales(char *, int[]);
 * void imprimir(int[]);
 * 
 * void main(){
 *      char *texto;
 *      int num[5];
 *      texto = pedirTexto();
 *      contarVocales(texto, num);
 *      imprimir(num);
 * }
 * 
 * A continuacion se explican cada una de las funciones llamadas desde main:
 * 1. La funcion pedirTexto devuelve un puntero a la cadena que el usuario ha escrito.
 * 2. La funcion contarVocales recibe el array que contiene el texto y otro array de
 *    enteros, en el cual va a devolver cuantas veces se repite cada una de las vocales,
 *    es decir, en num[0] vendran las veces que se repite la vocal a, en num[1] la vocal e
 *    y asi sucesivamente.
 * 3. La funcion imprimir recibe el array de enteros e imprime en la consola lo que ha encontrado.
*/

char *pedirTexto();
void contarVocales(char *, int[]);
void imprimir(int[]);

void main(){
    char *texto;
    int num[5];
    texto = pedirTexto();
    contarVocales(texto, num);
    imprimir(num); 
}

char *pedirTexto(){
    int LIMITE_TEXTO = 512;
    char *texto_ingresado = (char *)malloc(LIMITE_TEXTO*sizeof(char));
    printf("Ingrese el texto a procesar: ");
    scanf("%s", texto_ingresado);
    return texto_ingresado;
}

void contarVocales(char *texto, int *vocales){
    vocales = memcpy(vocales, (int[5]){0,0,0,0,0}, sizeof(int[5]));
    for (int i = 0; i < strlen(texto); i++){
        printf("%d\n", i);
        (texto[i] == 'a' || texto[i] == 'A') ? vocales[0]+=1:vocales[0];
        (texto[i] == 'e' || texto[i] == 'E') ? vocales[1]+=1:vocales[1];
        (texto[i] == 'i' || texto[i] == 'I') ? vocales[2]+=1:vocales[2];
        (texto[i] == 'o' || texto[i] == 'O') ? vocales[3]+=1:vocales[3];
        (texto[i] == 'u' || texto[i] == 'U') ? vocales[4]+=1:vocales[4];
    }
}

void imprimir(int vocales[]){
    printf("La cantidad de vocales en el texto son:\n");
    printf("Vocal\tCantidad\n");
    printf("a/A  \t%d\n", vocales[0]);
    printf("e/E  \t%d\n", vocales[1]);
    printf("i/I  \t%d\n", vocales[2]);
    printf("o/O  \t%d\n", vocales[3]);
    printf("u/U  \t%d\n", vocales[4]);
}