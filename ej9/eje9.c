#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Desarrolle un programa que permita al usuario introducir un numero determinado
 * de palabras y que de como resultado la impresion de las palabras ordenadas de forma
 * descendentes. Para ello se va utilizar la funcion strcmp(char*, char*) de la libreria
 * estandar de C que permite comparar dos cadenas de caracteres.
*/ 

void main(){
    int cantidad_palabras, error;
    char **cadena, *cadena_temp;
    printf("Ingrese la cantidad de palabras: ");
    scanf("%d", &cantidad_palabras);

    cadena = malloc(cantidad_palabras * sizeof(char *));
    for (int i = 0; i < cantidad_palabras; i++){
        cadena[i] = malloc((cantidad_palabras+1) * sizeof(char));
        printf("%d# Ingrese una palabra:", i);
        scanf("%s", cadena[i]);
    }

    cadena_temp = (char*) cadena;
    do{
        error = 0;
        for (int i = 0; i < cantidad_palabras - 1; i++){
            if (strcmp(cadena[i], cadena[i + 1]) > 0){
                strcpy(cadena_temp, cadena[i]);
                strcpy(cadena[i], cadena[i + 1]);
                strcpy(cadena[i + 1], cadena_temp);
                error = 1;
            }
        }

    } while (error == 1);
    printf("Palabras ordenados alfabeticamente:\n");
    for (int i = 0; i < cantidad_palabras; i++){
        printf("%d.%s\n", (i + 1), cadena[i]);
    }
}