#include <stdio.h>

int main()
{
    float n1;
    float n2;
    float *p1;
    float *p2;
    n1 = 4.0;
    p1 = &n1;
    p2 = p1;
    n2 = *p2;
    n1 = *p1 + *p2;
    printf("El valor de n1 es: %f y el valor de n2 es: %f \n", n1, n2);
    return 0;
}
