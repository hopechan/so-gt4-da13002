#include <stdio.h>
#include <stdlib.h>

/**
 * Desarrolle un programa que haciendo uso de la reserva dinamica de memoria
 * almacene un numero determinado de valores (obtenidos de forma aleatoria, entre 0 y 100)
 * y los ordene de mayor a menor
**/

void sort(int n, int *numeros){
    int temp;
    for (int i = 0; i < n; i++) { 
        for (int j = i + 1; j < n; j++) { 
            if(*(numeros + j) < *(numeros + i)){
                temp = *(numeros + i); 
                *(numeros + i) = *(numeros + j); 
                *(numeros + j) = temp; 
            }
        } 
    }
    printf("\033[1;32m"); // Color verde
    printf("Numeros despues de ordenarlos \n");
    for (int i = 0; i < n; i++) {
        printf(" %d ", *(numeros + i)); 
    } 
    printf("\n");       
}

int main(){
    int n, *total;
    printf("\nOrdenar numeros usando reserva dinamica de memoria :\n"); 
	printf("------------------------------------------------------\n");	  
    printf("Ingrese cuantos numeros desea ordenar: ");
    scanf("%d", &n);
    printf("------------------------------------------------------\n");	  

    // Se reserva memoria dinamicamente para almacenar n enteros
    total =(int* ) malloc(n *sizeof(int));

    // Se llena con valores aleatorios entre 0 y 100
    for (int i = 0; i < n; i++){
        total[i] = rand() % 100;
    }

    printf("\033[1;34m"); //Color azul
    printf("Numeros antes de ordenarlos: \n");
    for (int i = 0; i < n; i++){
        printf(" %d ", *(total + i));
    }

    printf("\n");
    sort(n, total);
    return 0;
}
