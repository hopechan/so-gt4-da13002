#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
    * Desarrolle un programa que permita evaluar un polinomio de cualquier grado:
    * P(X) = a0 + a1.x + a2.x^2 + a3.x^x3 + ... + an.x^n
    * Como no se sabe de que grado va a ser este polinomio, se debe crear una variable
    * puntero del tipo double:
    * double *coeficientes;
    * A continuacion se debe pedir el grado del polinomio y reservar memoria para el array
    * de coeficientes. Luego, a travez de un bucle se empezara a pedir coeficientes del polinomio.
    * Finalmente se debe pedir el punto (x) en que se va evaluar el polinomio, cuyo resultado se
    * sacara por consola. Tambien se debe sacar por la consola el polinomio que se ha evaluado.
*/

void constructor_polinomio(int grado, double punto, double *coeficientes);
double evaluador_polinomio(int grado, double punto, double *coeficientes);

int main(){
    int grado;
    double *coeficientes, punto;
    printf("\nEvaluador de polinomios\n"); 
	printf("--------------------------------------------------\n");	 
    printf("Ingrese el grado del polinomio: ");
    scanf("%d", &grado);
    coeficientes = (double* ) malloc(grado *sizeof(double));

    for (int i = 0; i <= grado; i++){
        printf("Ingrese el coeficiente para a%dx^%d: ", i, i);
        scanf("%lf", (coeficientes+i));
    }
    
    printf("\033[1;34m"); //Color azul
    printf("Ingrese el punto (x) a evaluar: ");
    scanf("%lf", &punto);

    printf("\033[0;35m");
    printf("El polinomio de grado %d que se forma es: \n", grado);
    constructor_polinomio(grado, punto, coeficientes);
    printf("Y el valor del polinomio en el punto %f es: %f\n", punto, evaluador_polinomio(grado, punto, coeficientes));
    return 0;
}

void constructor_polinomio(int grado, double punto, double *coeficientes){
    char *signo;
    for (int i = grado; i >= 0; i--){
        signo = ((char)signbit(*(coeficientes+i))==0) ? "+" : "-";
        printf("%c %fx^%d ", *signo, fabs(*(coeficientes+i)), i);   
    }
    printf("= 0\n");
    
}

double evaluador_polinomio(int grado, double punto, double *coeficientes){
    double multiplicador, acumulador = 0;
    for (int i = 0; i <= grado; i++){
        multiplicador = *(coeficientes + i)*pow(punto, i);
        acumulador += multiplicador;
    }
    return acumulador;
}
